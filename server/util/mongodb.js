const mongoClient = require('mongodb').MongoClient;

const uri = process.env.MONGODB_URI;
let mongodb;

function connect(callback) {
  mongoClient.connect(uri, (err, client) => {
    mongodb = client.db();
    console.log('MongoDB: Connected Successfully!');
    callback();
  });
}
function get() {
  return mongodb;
}

function close() {
  mongodb.close();
}

module.exports = {
  connect,
  get,
  close
};
