const express = require('express');
const appController = require('../controller/appController');

const router = express.Router();

router.get('/get/personalInfo', appController.getPersonalInfo);
router.get('/get/skills', appController.getSkills);
router.get('/get/experience', appController.getExperience);
router.get('/get/projects', appController.getProjects);

module.exports = router;
