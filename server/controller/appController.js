const db = require('../util/mongodb');

const getPersonalInfo = (req, res) => {
  db.get()
    .collection('about')
    .findOne({})
    .then(personalInfo => res.send(personalInfo));
};

const getSkills = (req, res) => {
  db.get()
    .collection('skills')
    .find({})
    .toArray()
    .then(skills => res.send(skills));
};

const getExperience = (req, res) => {
  db.get()
    .collection('experience')
    .aggregate(
      [
        { $sort: { from: -1 } }
      ]
    )
    .toArray()
    .then(experience => res.send(experience));
};

const getProjects = (req, res) => {
  db.get()
    .collection('projects')
    .find({})
    .toArray()
    .then(projects => res.send(projects));
};

module.exports = {
  getPersonalInfo,
  getSkills,
  getExperience,
  getProjects
};
