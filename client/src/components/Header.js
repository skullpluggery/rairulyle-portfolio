import React, { Component } from 'react';
import M from 'materialize-css';
import PropTypes from 'prop-types';

class Header extends Component {
  componentDidMount() {
    M.Parallax.init(document.querySelectorAll('.parallax'), {});
  }

  render() {
    const { personalInfo } = this.props;
    return (
      <div className='header-section'>
        <div className='parallax-container'>
          <div className='parallax'>
            <img className='responsive-img' alt='Header' src='/images/header.jpeg' />
          </div>
        </div>
        <div className='container header-content'>
          <div className='row no-padding'>
            <div className='col s12'>
              <img
                src='/images/primary-picture.jpeg'
                alt='Primary'
                className='valign center-block circle primary-picture'
              />
            </div>
            <div className='col s12 center'>
              <h5>
                Konnichiwassup! I’m
                <span className='crimson-red-text'>
                  {` ${personalInfo.firstName} ${personalInfo.lastName}`}
                </span>
                , a<span className='crimson-red-text'> Front-end Software Engineer </span>
                living in {`${personalInfo.location}`}, and currently working
                for
                <span className='crimson-red-text'>
                  {' '}
                  Manulife
                </span>
                .
              </h5>
            </div>
            <div className='col s12 header-buttons center'>
              <a
                href={personalInfo.social.linkedIn}
                target='_blank'
                rel='noopener noreferrer'
                className='waves-effect waves-light btn'
              >
                <i className='mdi mdi-linkedin' />
              </a>
              <a
                href={personalInfo.social.gdriveCV}
                target='_blank'
                rel='noopener noreferrer'
                className='waves-effect waves-light btn'
              >
                <i className='mdi mdi-file-account-outline left' />
                VIEW MY CV
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  personalInfo: PropTypes.object
};

export default Header;
