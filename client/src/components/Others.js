import React, { Component } from 'react';
import dateFormatter from '../util/dateFormatter';
import Illustration from './common/Illustration';

class Others extends Component {
  render() {
    return (
      <div>
        <div id='others' className='section center'>
          <h4 className='heading-title'>
            <strong>OTHERS</strong>
          </h4>
          <div className='strong-divider' />
          <Illustration data='others' />
          <h5 className='crimson-red-text'>
            <strong>Publication</strong>
          </h5>
          <h6>
            <strong>
              Multi Zone-Based Surface Air Quality Monitoring via Internet of
              Things
            </strong>
          </h6>
          <h6>
            Journal of Communications{' '}
            <span className='experience-date'>
              <i className='mdi mdi-calendar crimson-red-text' />
              {dateFormatter.formatDate('201906')}
            </span>
          </h6>
          <p>
            This study aimed to design and create a system that could monitor
            the surface air quality from the different distant zones of a local
            municipality. The primary focus was to develop a cost-effective and
            portable air quality monitoring system that can hook up online and
            equipped with sensory system of major pollutants namely Carbon
            Monoxide, Nitrogen Dioxide, and Sulfur Dioxide.
          </p>
          <a
            href='http://bit.ly/AirQualityMonitoringSystemMCL'
            target='_blank'
            rel='noopener noreferrer'
            className='btn waves-effect waves-light'
          >
            <i className='mdi mdi-file-certificate left' />
            SEE PUBLICATION
          </a>
        </div>
      </div>
    );
  }
}

export default Others;
