import React from 'react';
import PropTypes from 'prop-types';
import '../../assets/scss/timeline.scss';
import dateFormatter from '../../util/dateFormatter';

const Timeline = props => {
  const { experience } = props;

  let indicatorElement = null;

  const timelineEventElement = experience.map((expi, index) => {
    if (expi.isCurrent) {
      indicatorElement = <div className='timeline-badge crimson-red' />;
    } else {
      indicatorElement = <div className='timeline-badge crimson-black' />;
    }

    const yearsOfStay =
      expi.to === 'Present'
        ? ``
        : `(${dateFormatter.getYearsOfStay(expi.from, expi.to)})`;

    return (
      <div key={index} className='timeline-event'>
        <div className='timeline-content'>
          <h5 className='job-position'>
            <strong>{expi.position}</strong>
          </h5>
          <h6 className='company-name'>
            <a
              className='crimson-red-text'
              target='_blank'
              rel='noopener noreferrer'
              href={expi.link}
            >
              <strong>{expi.company}</strong>
            </a>
          </h6>
          <span className='experience-date'>
            <i className='mdi mdi-calendar crimson-red-text' />
            {`${dateFormatter.formatDate(
              expi.from
            )} - ${dateFormatter.formatDate(expi.to)} ${yearsOfStay}`}
          </span>
        </div>
        {indicatorElement}
      </div>
    );
  });

  return (
    <div>
      <div className='timeline'>{timelineEventElement}</div>
    </div>
  );
};

Timeline.propTypes = {
  experience: PropTypes.array
};

export default Timeline;
