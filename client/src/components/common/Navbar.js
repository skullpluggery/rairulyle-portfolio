import React, { Component } from 'react';
import M from 'materialize-css';

class Navbar extends Component {
  componentDidMount() {
    const elems = document.querySelectorAll('.sidenav');
    M.Sidenav.init(elems, {});

    const nav = document.getElementById('nav');

    const navTransition = () => {
      if (window.scrollY <= 136) {
        nav.classList.add('transparent');
        nav.classList.add('z-depth-0');
        nav.classList.remove('crimson-black');
      } else {
        nav.classList.add('crimson-black');
        nav.classList.remove('transparent');
        nav.classList.remove('z-depth-0');
      }
    };

    window.addEventListener('scroll', navTransition);
  }

  render() {
    const menuItems = [
      {
        title: 'SKILLS',
        link: '#skills'
      },
      {
        title: 'EXPERIENCE',
        link: '#experience'
      },
      {
        title: 'PROJECTS',
        link: '#projects'
      },
      {
        title: 'OTHERS',
        link: '#others'
      },
      {
        title: 'CONNECT',
        link: '#connect'
      }
    ];

    const menuItemsElement = menuItems.map((item, index) => {
      return (
        <li key={index}>
          <a href={item.link}>{item.title}</a>
        </li>
      );
    });

    return (
      <div className='navbar-fixed'>
        <nav id='nav' className='transparent z-depth-0'>
          <div className='nav-wrapper container'>
            <a href='http://www.rairulyle.me' className='brand-logo svg'>
              <object data='/images/rairu-logo.svg' type='image/svg+xml' aria-label='Logo'>
                <span>Your browser doesn&apos;t support SVG images</span>
              </object>
            </a>
            <a href='#' data-target='mobile-menu' className='sidenav-trigger'>
              <i className='mdi mdi-menu' />
            </a>
            <ul id='nav-mobile' className='right hide-on-med-and-down'>
              {menuItemsElement}
            </ul>

            <ul className='sidenav' id='mobile-menu'>
              {menuItemsElement}
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
