import React from 'react';
import PropTypes from 'prop-types';

const Illustration = props => {
  const { data } = props;

  return (
    <div className='illustration'>
      <object
        data={`images/illustrations/${data}.svg`}
        type='image/svg+xml'
        aria-label='Illustration'
      >
        <span>Your browser doesn&apos;t support SVG images</span>
      </object>
    </div>
  );
};

Illustration.propTypes = {
  data: PropTypes.string
};

export default Illustration;
