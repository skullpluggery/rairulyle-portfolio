import React from 'react';
import PropTypes from 'prop-types';

const ProjectCard = props => {
  const { projects } = props;
  const projectImageDir = '/images/projects/';

  const projectElement = projects.map((project, index) => {
    const techUsedElement = project.techs.map((tech, chipIndex) => {
      return (
        <div className='chip' key={chipIndex}>
          {tech}
        </div>
      );
    });

    return (
      <div key={index} className='col s12 m6'>
        <div className='project card hoverable'>
          <div className='card-image'>
            <img src={projectImageDir + project.img} alt='project' />
          </div>
          <div className='card-content'>
            <span className='card-title crimson-red-text'>
              <strong>{project.title}</strong>
            </span>
            <p>{project.description}</p>
            <div className='powered-by'>{techUsedElement}</div>
          </div>
          <div className='card-action'>
            {project.demo && (
              <a
                href={project.demo}
                target='_blank'
                rel='noopener noreferrer'
                className='btn waves-effect waves-light'
              >
                <i className='mdi mdi-open-in-app left' />
                DEMO
              </a>
            )}
            {project.source && (
              <a
                href={project.source}
                target='_blank'
                rel='noopener noreferrer'
                className='btn waves-effect waves-light crimson-black'
              >
                <i className='mdi mdi-source-repository left' />
                SOURCE
              </a>
            )}
          </div>
        </div>
      </div>
    );
  });

  return <div>{projectElement}</div>;
};

ProjectCard.propTypes = {
  projects: PropTypes.array
};

export default ProjectCard;
