import React, { Component } from 'react';

class ScrollToTopButton extends Component {
  componentDidMount() {
    const scrollToTopButton = document.getElementById('scroll-to-top');

    const showScrollToTopButton = () => {
      if (window.scrollY <= 236) {
        scrollToTopButton.style.visibility = 'hidden';
        scrollToTopButton.style.opacity = 0;
      } else {
        scrollToTopButton.style.visibility = 'visible';
        scrollToTopButton.style.opacity = 1;
      }
    };

    window.addEventListener('scroll', showScrollToTopButton);
  }

  render() {
    return (
      <div>
        <button
          type='button'
          id='scroll-to-top'
          className='btn-floating btn-large waves-effect waves-light'
          onClick={() => {
            window.scrollTo(0, 0);
          }}
        >
          <i className='mdi mdi-arrow-up' />
        </button>
      </div>
    );
  }
}

export default ScrollToTopButton;
