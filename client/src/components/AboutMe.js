import React, { Component } from 'react';

class AboutMe extends Component {
  render() {
    return (
      <div>
        <div id='about-me' className='section'>
          <h4 className='heading-title'>
            <strong>ABOUT ME</strong>
          </h4>
          <div className='strong-divider' />
        </div>
      </div>
    );
  }
}

export default AboutMe;
