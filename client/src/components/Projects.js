import React from 'react';
import PropTypes from 'prop-types';
import ProjectCard from './common/ProjectCard';
import Illustration from './common/Illustration';

const Projects = props => {
  const { projects } = props;

  return (
    <div>
      <div id='projects' className='section center'>
        <h4 className='heading-title'>
          <strong>PROJECTS</strong>
        </h4>
        <div className='strong-divider' />
        <Illustration data='projects' />
        <div className='row project-list'>
          <ProjectCard projects={projects} />
        </div>
      </div>
    </div>
  );
};

Projects.propTypes = {
  projects: PropTypes.array
};

export default Projects;
