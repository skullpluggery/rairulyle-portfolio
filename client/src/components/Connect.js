import React from 'react';
import PropTypes from 'prop-types';
import Illustration from './common/Illustration';

const Connect = props => {
  const { personalInfo } = props;
  return (
    <div>
      <div id='connect' className='section center'>
        <h4 className='heading-title'>
          <strong>CONNECT</strong>
        </h4>
        <div className='strong-divider' />
        <Illustration data='connect' />
        <h5>
          Got an awesome idea? Let&apos;s have a{' '}
          <a className='crimson-red-text' href={`mailto: ${personalInfo.email}`}>
            chat!
          </a>
        </h5>

        <ul className='connect'>
          <li>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href={personalInfo.social.linkedIn}
            >
              <i className='mdi mdi-linkedin-box' />
              LinkedIn
            </a>
          </li>
          <li>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href={personalInfo.social.gitlab}
            >
              <i className='mdi mdi-gitlab' />
              GitLab
            </a>
          </li>
          <li>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href={personalInfo.social.github}
            >
              <i className='mdi mdi-github-box' />
              GitHub
            </a>
          </li>
          <li>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href={personalInfo.social.facebook}
            >
              <i className='mdi mdi-facebook-box' />
              Facebook
            </a>
          </li>
          <li>
            <a
              target='_blank'
              rel='noopener noreferrer'
              href={personalInfo.social.instagram}
            >
              <i className='mdi mdi-instagram' />
              Instagram
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

Connect.propTypes = {
  personalInfo: PropTypes.object
};

export default Connect;
