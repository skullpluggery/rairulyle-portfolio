import React from 'react';
import PropTypes from 'prop-types';
import Timeline from './common/Timeline';
import Illustration from './common/Illustration';

const Experience = props => {
  const { experience } = props;
  return (
    <div>
      <div id='experience' className='section center'>
        <h4 className='heading-title'>
          <strong>EXPERIENCE</strong>
        </h4>
        <div className='strong-divider' />
        <Illustration data='experience' />
        <Timeline experience={experience} />
      </div>
    </div>
  );
};

Experience.propTypes = {
  experience: PropTypes.array
};

export default Experience;
