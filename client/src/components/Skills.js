import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Illustration from './common/Illustration';

class Skills extends Component {
  render() {
    const { skills } = this.props;

    const coding = skills.find(skill => skill.type === 'coding');
    const design = skills.find(skill => skill.type === 'design');
    const application = skills.find(skill => skill.type === 'application');
    const methodology = skills.find(skill => skill.type === 'methodology');

    return (
      <div>
        <div id='skills' className='section center'>
          <h4 className='heading-title'>
            <strong>SKILLS</strong>
          </h4>
          <div className='strong-divider' />
          <Illustration data='skills' />
          <h5 className='crimson-red-text'>
            <strong>Coding</strong>
          </h5>
          {coding.list.map((skill, index) => (
            <div className='chip' key={index}>
              {skill}
            </div>
          ))}

          <h5 className='crimson-red-text'>
            <strong>Design Language</strong>
          </h5>
          {design.list.map((skill, index) => (
            <div className='chip' key={index}>
              {skill}
            </div>
          ))}

          <h5 className='crimson-red-text'>
            <strong>Application</strong>
          </h5>
          {application.list.map((skill, index) => (
            <div className='chip' key={index}>
              {skill}
            </div>
          ))}

          <h5 className='crimson-red-text'>
            <strong>Development Methodology</strong>
          </h5>
          {methodology.list.map((skill, index) => (
            <div className='chip' key={index}>
              {skill}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

Skills.propTypes = {
  skills: PropTypes.array
};

export default Skills;
