import portfolio from '../../api/portfolio';
import * as types from '../constants/actionTypes';

const fetchPersonalInfo = data => ({
  type: types.FETCH_PERSONAL_INFO,
  data
});

export const getPersonalInfo = () => {
  return dispatch => {
    return portfolio
      .getPersonalInfo()
      .then(res => dispatch(fetchPersonalInfo(res.data.personalInfo)));
  };
};

const fetchSkills = data => ({
  type: types.FETCH_SKILLS,
  data
});

export const getSkills = () => {
  return dispatch => {
    return portfolio.getSkills().then(res => dispatch(fetchSkills(res.data)));
  };
};

const fetchExperience = data => ({
  type: types.FETCH_EXPERIENCE,
  data
});

export const getExperience = () => {
  return dispatch => {
    return portfolio.getExperience().then(res => dispatch(fetchExperience(res.data)));
  };
};

const fetchProjects = data => ({
  type: types.FETCH_PROJECTS,
  data
});

export const getProjects = () => {
  return dispatch => {
    return portfolio.getProjects().then(res => dispatch(fetchProjects(res.data)));
  };
};
