import * as types from '../constants/actionTypes';

const initialState = {
  personalInfo: null,
  skills: null,
  experience: null,
  projects: null,
  isFetching: false,
  isError: false
};

const portfolioReducer = (state = initialState, action) => {
  const { data } = action;
  switch (action.type) {
    case types.FETCH_PERSONAL_INFO:
      return {
        ...state,
        personalInfo: data
      };
    case types.FETCH_SKILLS:
      return {
        ...state,
        skills: data
      };
    case types.FETCH_EXPERIENCE:
      return {
        ...state,
        experience: data
      };
    case types.FETCH_PROJECTS:
      return {
        ...state,
        projects: data
      };
    default:
      return state;
  }
};

export default portfolioReducer;
