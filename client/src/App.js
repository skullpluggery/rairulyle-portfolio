import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import 'materialize-css/dist/css/materialize.min.css';
import '@mdi/font/css/materialdesignicons.min.css';
import 'typeface-montserrat';
import './assets/css/color.min.css';
import './App.css';
import Navbar from './components/common/Navbar';
import ScrollToTopButton from './components/common/ScrollToTopButton';
import Header from './components/Header';
import Skills from './components/Skills';
import Experience from './components/Experience';
import Projects from './components/Projects';
import Others from './components/Others';
import AboutMe from './components/AboutMe';
import Connect from './components/Connect';

class App extends Component {
  render() {
    const { personalInfo, skills, experience, projects } = this.props;

    return (
      <div className='App'>
        <Navbar />
        {personalInfo ? <Header personalInfo={personalInfo} /> : null}
        <div className='container'>
          <div className='divider' />
          {skills ? <Skills skills={skills} /> : null}
          {experience ? <Experience experience={experience} /> : null}
          {projects ? <Projects projects={projects} /> : null}
          <Others />
          {/* 
          <AboutMe /> */}
          {personalInfo ? <Connect personalInfo={personalInfo} /> : null}
        </div>
        <ScrollToTopButton />
      </div>
    );
  }
}

App.propTypes = {
  experience: PropTypes.any,
  personalInfo: PropTypes.any,
  skills: PropTypes.any,
  projects: PropTypes.any
};

const mapStateToProps = store => {
  const { personalInfo, skills, experience, projects } = store.portfolioReducer;
  return {
    personalInfo,
    skills,
    experience,
    projects
  };
};

export default connect(mapStateToProps)(App);
