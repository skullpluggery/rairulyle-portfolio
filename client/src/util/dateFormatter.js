import moment from 'moment';

const dateFormatter = {
  getYearsOfStay: (from, to) => {
    const fromDate = moment(from, 'YYYYMM');
    const toDate = moment(to, 'YYYYMM');

    const years = toDate.diff(fromDate, 'year');
    fromDate.add(years, 'years');

    const months = toDate.diff(fromDate, 'months') + 1;

    let stringYearsOfStay = '';

    if (years > 1) {
      stringYearsOfStay += `${years} years`;
    } else if (years === 1) {
      stringYearsOfStay += `${years} year`;
    }

    if (years !== 0 && months !== 0) {
      stringYearsOfStay += ` and `;
    }

    if (months > 1) {
      stringYearsOfStay += `${months} months`;
    } else if (months === 1) {
      stringYearsOfStay += `${months} month`;
    }
    
    return stringYearsOfStay;
  },
  formatDate: date =>
    date !== 'Present' ? moment(date, 'YYYYMM').format('MMM YYYY') : date
};

export default dateFormatter;
