import axios from 'axios';

const baseURL =
  process.env.NODE_ENV === 'production'
    ? `/`
    : `http://localhost:3000/`;

const portfolioService = {
  getPersonalInfo: () => axios.get(`${baseURL}get/personalInfo`),
  getSkills: () => axios.get(`${baseURL}get/skills`),
  getExperience: () => axios.get(`${baseURL}get/experience`),
  getProjects: () => axios.get(`${baseURL}get/projects`),
};

export default portfolioService;
